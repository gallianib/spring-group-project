package GroupProject;

import GroupProject.GroupApp.*;
import GroupProject.Users.*;
import GroupProject.Users.UsersRepository;
import org.junit.*;
import org.junit.After;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.internal.verification.VerificationModeFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.restdocs.JUnitRestDocumentation;
import org.springframework.restdocs.templates.TemplateFormats;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

import static org.springframework.restdocs.hypermedia.HypermediaDocumentation.linkWithRel;
import static org.springframework.restdocs.hypermedia.HypermediaDocumentation.links;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.requestParameters;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.mockito.Mockito.when;
import org.junit.Before;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import static org.junit.Assert.assertEquals;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.org.apache.xalan.internal.xsltc.dom.LoadDocument;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.hateoas.MediaTypes;
import org.springframework.restdocs.JUnitRestDocumentation;
import org.springframework.restdocs.config.RestDocumentationConfigurer;
import org.springframework.restdocs.constraints.ConstraintDescriptions;
import org.springframework.restdocs.headers.HeaderDocumentation;
import org.springframework.restdocs.mockmvc.MockMvcOperationPreprocessorsConfigurer;
import org.springframework.restdocs.mockmvc.MockMvcRestDocumentation;
import org.springframework.restdocs.mockmvc.MockMvcSnippetConfigurer;
import org.springframework.restdocs.mockmvc.RestDocumentationResultHandler;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import javax.servlet.RequestDispatcher;
import java.util.HashMap;
import java.util.Map;
import static java.util.Collections.singletonList;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.springframework.restdocs.headers.HeaderDocumentation.headerWithName;
import static org.springframework.restdocs.headers.HeaderDocumentation.responseHeaders;
import static org.springframework.restdocs.hypermedia.HypermediaDocumentation.linkWithRel;
import static org.springframework.restdocs.hypermedia.HypermediaDocumentation.links;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.*;
import static org.springframework.restdocs.payload.PayloadDocumentation.*;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.pathParameters;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.util.StringUtils.collectionToDelimitedString;

@RunWith(SpringRunner.class)
//@RunWith(SpringRunner.class)
@SpringBootTest
public class RESTDocUnitTest {

        abstract class MockMvcRestDocumentationConfigurer
                extends RestDocumentationConfigurer<org.springframework.restdocs.config.AbstractConfigurer,MockMvcRestDocumentationConfigurer>
                implements org.springframework.test.web.servlet.setup.MockMvcConfigurer{}

        @Rule
        public final JUnitRestDocumentation restDocumentation = new JUnitRestDocumentation("target/generated-snippets");

        public RESTDocUnitTest(){
            System.out.println("Test class created");
        }

        @Autowired
        private WebApplicationContext context;

        @Autowired
        private ObjectMapper objectMapper;

        @Autowired
        private MainController control;

        private MockMvc mockMvc;
        private Users user1;
        private RestDocumentationResultHandler documentationHandler;

        @MockBean
        private UsersRepository userRepo;

        @Before
        public void setUp() {
            mockMvc = MockMvcBuilders.standaloneSetup(control).apply(documentationConfiguration(this.restDocumentation))
                    .build();
            user1 = new Users();
            user1.setUid(9L);
            user1.setName("John Doe");
            user1.setPassword("xyz");
            Optional<Users> opt= Optional.of(user1);
            when(userRepo.findById(1L)).thenReturn(Optional.ofNullable(user1));

        }
        @Before
        public void setUp1() {

            this.mockMvc = MockMvcBuilders.webAppContextSetup(this.context)
                    .apply(documentationConfiguration(this.restDocumentation))
                    .alwaysDo(document("{method-name}", preprocessRequest(prettyPrint()), preprocessResponse(prettyPrint())))
                    .build();
        }

        @Before
        public void before() {
            System.out.println("Before cases");
        }
        @After
        public void after() {
            System.out.println("After cases");
        }

        @BeforeClass
        public static void beforeClass() {
            System.out.println("Before test Class ");
        }
        @AfterClass
        public static void afterClass() {
            System.out.println("After test Class");
        }

       /* @Before
        public void setUp() {
            this.documentationHandler = document("{method-name}",
                    preprocessRequest(prettyPrint()),
                    preprocessResponse(prettyPrint()));

            this.mockMvc = MockMvcBuilders.webAppContextSetup(this.context)
                    .apply(documentationConfiguration(this.restDocumentation))
                    .alwaysDo(this.documentationHandler)
                    .build();
        }
        @Test
        public void indexExample() throws Exception {
            this.mockMvc.perform(get("/"))
                    .andExpect(status().isOk())
                    .andDo(document("index-example", preprocessRequest(prettyPrint()), preprocessResponse(prettyPrint()), links(linkWithRel("GroupProject").description("The CRUD resource")),
                            responseHeaders(headerWithName("Content-Type").description("The Content-Type of the payload, e.g. `application/hal+json`"))));
        }*/

        @Test
        public void contextLoads() {
            assertThat(control).isNotNull();
    }

        @Test
        public void shouldReturnDefaultMessage() throws Exception {
            this.mockMvc.perform(get("/index.html")).andDo(print()).andExpect(status().isOk())
                    .andExpect(content().string(containsString("")));
        }


        @Test
        public void testIndex() throws Exception{
            this.mockMvc.perform(get("/static/index.html"))
                    .andExpect(status().isOk())
                    .andExpect(content().string(containsString("")))
                    .andDo(print())
                    .andDo(document("Logon Screen"))
            ;
        }

      /*  @Test
        public void testGetUser() throws Exception{

            this.mockMvc.perform(user1.setGid(123L)
                    .contentType(MediaType.APPLICATION_JSON)
                    .andExpect(status().isOk())
                    .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                    //.andExpect(jsonPath("$.name",is(user1.getName())))
                    .andDo(print())
                    .andDo(document("user", responseFields(
                            fieldWithPath("password").description("The user's pw"),
                            fieldWithPath("name").description("The user's name"),
                            fieldWithPath("gid").description("The user's id"))))
            ;
        } */

        @Test
        public void testGetAllUser() throws Exception{
            Users user2 = new Users();
            user2.setUid(7L);
            user2.setName("Tayler Swift");
            user2.setPassword("123");
            List<Users> allUsers = Arrays.asList(user1, user2);
            given(userRepo.findAll()).willReturn(allUsers);
            this.mockMvc.perform(get("/allUsers"))

                    .andExpect(status().isOk())
                    .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                    .andExpect(jsonPath("$", hasSize(2)))

                    //.andExpect(jsonPath("$[0].name",is(user1.getName())))
                    //.andExpect(jsonPath("$[1].name",is(user2.getName())))
                    .andDo(print());
            verify(userRepo, VerificationModeFactory.times(1)).findAll();
        }

        @Test
        public void crudGetExample() throws Exception {

            Map<String, Object> crud = new HashMap<>();
            crud.put("id", 1L);
            crud.put("title", "Sample Model");
            crud.put("body", "http://localport");

            String tagLocation = this.mockMvc.perform(get("/GroupProject").contentType(MediaTypes.HAL_JSON)
                    .content(this.objectMapper.writeValueAsString(crud)))
                    .andExpect(status().isOk())
                    .andReturn()
                    .getResponse()
                    .getHeader("Location");

            crud.put("tags", singletonList(tagLocation));

            ConstraintDescriptions desc = new ConstraintDescriptions(Users.class);

            this.mockMvc.perform(get("/GroupProject").contentType(MediaTypes.HAL_JSON)
                    .content(this.objectMapper.writeValueAsString(crud)))
                    .andExpect(status().isOk())
                    .andDo(document("crud-get-example", preprocessRequest(prettyPrint()), preprocessResponse(prettyPrint()), requestFields(fieldWithPath("id").description("The id of the input" + collectionToDelimitedString(desc.descriptionsForProperty("id"), ". ")),
                            fieldWithPath("title").description("The title of the input"), fieldWithPath("body").description("The body of the input"), fieldWithPath("tags").description("An array of tag resource URIs"))));
        }
        @Test
        public void headersExample() throws Exception {
            this.mockMvc
                    .perform(get("/"))
                    .andExpect(status().isOk())
                    .andDo(this.documentationHandler.document(
                            responseHeaders(
                                    headerWithName("Content-Type").description("The Content-Type of the payload, e.g. `application/hal+json`"))));
        }
        @Test
        public void errorExample() throws Exception {
            this.mockMvc
                    .perform(get("/error")
                            .requestAttr(RequestDispatcher.ERROR_STATUS_CODE, 400)
                            .requestAttr(RequestDispatcher.ERROR_REQUEST_URI,
                                    "/notes")
                            .requestAttr(RequestDispatcher.ERROR_MESSAGE,
                                    "The tag 'http://localhost:8080/tags/123' does not exist"))
                    .andDo(print()).andExpect(status().isBadRequest())
                    //.andExpect(jsonPath("error", is("Bad Request")))
                    //.andExpect(jsonPath("timestamp", is(notNullValue())))
                    //.andExpect(jsonPath("status", is(400)))
                    //.andExpect(jsonPath("path", is(notNullValue())))
                    .andDo(document("error-example",
                            responseFields(
                                    fieldWithPath("error").description("The HTTP error that occurred, e.g. `Bad Request`"),
                                    fieldWithPath("message").description("A description of the cause of the error"),
                                    fieldWithPath("path").description("The path to which the request was made"),
                                    fieldWithPath("status").description("The HTTP status code, e.g. `400`"),
                                    fieldWithPath("timestamp").description("The time, in milliseconds, at which the error occurred"))));
        }
        @Test
        public void crudCreateExample() throws Exception {
            Map<String, Object> crud = new HashMap<>();
            crud.put("id", 2L);
            crud.put("title", "Sample Model");
            crud.put("body", "http://localport/");

            String tagLocation = this.mockMvc.perform(post("/GroupProject").contentType(MediaTypes.HAL_JSON)
                    .content(this.objectMapper.writeValueAsString(crud)))
                    .andExpect(status().isCreated())
                    .andReturn()
                    .getResponse()
                    .getHeader("Location");

            crud.put("tags", singletonList(tagLocation));

            this.mockMvc.perform(post("/GroupProject").contentType(MediaTypes.HAL_JSON)
                    .content(this.objectMapper.writeValueAsString(crud)))
                    .andExpect(status().isCreated())
                    .andDo(document("crud-create-example", preprocessRequest(prettyPrint()), preprocessResponse(prettyPrint()), requestFields(fieldWithPath("id").description("The id of the input"), fieldWithPath("title").description("The title of the input"),
                            fieldWithPath("body").description("The body of the input"), fieldWithPath("tags").description("An array of tag resource URIs"))));
        }

        @Test
        public void crudDeleteExample() throws Exception {
            this.mockMvc.perform(delete("/GroupProject/{id}", 10))
                    .andExpect(status().isOk())
                    .andDo(document("crud-delete-example", pathParameters(parameterWithName("id").description("The id of the input to delete"))));
        }

        @Test
        public void crudPatchExample() throws Exception {

            Map<String, String> tag = new HashMap<>();
            tag.put("name", "PATCH");

            String tagLocation = this.mockMvc.perform(patch("/GroupProject/{id}", 10).contentType(MediaTypes.HAL_JSON)
                    .content(this.objectMapper.writeValueAsString(tag)))
                    .andExpect(status().isOk())
                    .andReturn()
                    .getResponse()
                    .getHeader("Location");

            Map<String, Object> crud = new HashMap<>();
            crud.put("title", "Sample Model Patch");
            crud.put("body", "http://Localport");
            crud.put("tags", singletonList(tagLocation));

            this.mockMvc.perform(patch("/GroupProject/{id}", 10).contentType(MediaTypes.HAL_JSON)
                    .content(this.objectMapper.writeValueAsString(crud)))
                    .andExpect(status().isOk());
        }

        @Test
        public void crudPutExample() throws Exception {
            Map<String, String> tag = new HashMap<>();
            tag.put("name", "PUT");

            String tagLocation = this.mockMvc.perform(put("/GroupProject/{id}", 10).contentType(MediaTypes.HAL_JSON)
                    .content(this.objectMapper.writeValueAsString(tag)))
                    .andExpect(status().isAccepted())
                    .andReturn()
                    .getResponse()
                    .getHeader("Location");

            Map<String, Object> crud = new HashMap<>();
            crud.put("title", "Sample Model");
            crud.put("body", "http://Localport");
            crud.put("tags", singletonList(tagLocation));

            this.mockMvc.perform(put("/GroupProject/{id}", 10).contentType(MediaTypes.HAL_JSON)
                    .content(this.objectMapper.writeValueAsString(crud)))
                    .andExpect(status().isAccepted());
        }
}