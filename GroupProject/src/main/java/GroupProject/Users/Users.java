package GroupProject.Users;

import javax.persistence.*;

@Entity
@Table(name = "UsersTable")
public class Users {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "uid", updatable = false, nullable = false)
    private Long uid;
    private String name;
    private String password;
    private boolean inGroup;

    public boolean isInGroup() {
        return inGroup;
    }

    public void setInGroup(boolean inGroup) {
        this.inGroup = inGroup;
    }

    public String getPassword() { return password; }

    public void setPassword(String password) { this.password = password; }

    public Long getUid() {
        return uid;
    }

    public void setUid(Long id) {
        this.uid = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
