package GroupProject;

import GroupProject.Credentials.Credentials;
import GroupProject.Credentials.CredentialsRepository;
import GroupProject.Encoders.CredentialEncoder;
import GroupProject.Encoders.Encryptor;
import GroupProject.Encoders.GroupPasswordEncoder;
import GroupProject.Groups.Groups;
import GroupProject.Groups.GroupsRepository;
import GroupProject.Groups.UsersGroups;
import GroupProject.Groups.UsersGroupsRepository;
import GroupProject.Users.Users;
import GroupProject.Users.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
public class MainController {
    @Autowired
    private UsersGroupsRepository usersGroupsRepository;
    @Autowired
    private UsersRepository usersRepository;
    @Autowired
    private GroupsRepository groupsRepository;
    @Autowired
    private CredentialsRepository credentialsRepository;
    @Autowired
    private GroupPasswordEncoder passwordEncoder;

    @PostMapping("/login")
    public String login(@RequestParam(name="name") String name,
                        @RequestParam(name="password") String password, Model model, HttpServletRequest request, HttpServletResponse response) {
        Iterable<Users> all = usersRepository.findAll();
        for (Users u:all) {
            if (name.equals(u.getName()) && passwordEncoder.matches(password,u.getPassword())){
                model.addAttribute("name", name);
                response.addCookie(new Cookie("uid",u.getUid().toString()));
                if (u.isInGroup()) {
                    String out = getAllCredsInGroups(u.getUid());
                    model.addAttribute("groupsInfo",out);
                    return "group";
                }else return "user";
            }
        }
        model.addAttribute("error","Failed To Login!");
        return "errorpage";
    }
    @RequestMapping("/createNewUser")
    public String sendToCreateUser(){
        return "createNewUser";
    }
    @RequestMapping("/allUsers")
    public @ResponseBody List<Users> findAllUsers(){
        return (List<Users>)usersRepository.findAll();
    }
    @RequestMapping("/allUsersGroups")
    public @ResponseBody List<UsersGroups> findAllUsersGroups(){
        return (List<UsersGroups>)usersGroupsRepository.findAll();
    }
    @RequestMapping("/allGroups")
    public @ResponseBody List<Groups> findAllGroups(){
        return (List<Groups>)groupsRepository.findAll();
    }
    @RequestMapping("/allCreds")
    public @ResponseBody List<Credentials> findAllCreds(){
        return (List<Credentials>)credentialsRepository.findAll();
    }

    @RequestMapping("/newUser")
    public String createNewUser(@RequestParam(name="name") String name,
                                @RequestParam(name="password") String password, Model model, HttpServletRequest request, HttpServletResponse response){
        Iterable<Users> all = usersRepository.findAll();
        for (Users u:all) {
            if (name.equals(u.getName())){
                model.addAttribute("error","User Already Exists!");
                return "errorpage";
            }
        }
        Users u = new Users();
        u.setName(name);
        u.setPassword(passwordEncoder.encode(password));
        usersRepository.save(u);
        response.addCookie(new Cookie("uid",u.getUid().toString()));
        model.addAttribute("name",name);
        return "user";
    }
    @RequestMapping("/newCredential")
    public String createNewCredential(@CookieValue(name = "uid")Long uid,
                                      @RequestParam(name="group")String group,
                                      @RequestParam(name="name") String name,
                                      @RequestParam(name="password") String password,
                                      @RequestParam(name="desc") String desc, Model model, HttpServletRequest request, HttpServletResponse response){
        Iterable<Credentials> all = credentialsRepository.findAll();
        Iterable<Groups> allGroups = groupsRepository.findAll();
        Long gid = null;
        for (Groups g:allGroups) {
            if (g.getGroupName().equals(group)){
                gid = g.getGid();
            }
        }
        for (Credentials c:all) {
            if (c.getGid().equals(gid) && gid != null) {
                if (name.equals(c.getCredentialName())) {
                    model.addAttribute("error", "Credential Already Exists!");
                    return "errorpage";
                }
            }
        }
        try {
            Credentials cred = new Credentials();
            cred.setGid(gid);
            cred.setCredentialName(name);
            cred.setCredentialPassword(Encryptor.encrypt("Bar12345Bar12345","RandomInitVector",password));
            cred.setDescription(desc);
            credentialsRepository.save(cred);
        }catch(Exception e){
            model.addAttribute("error", "Something Went Wrong!");
            return "errorpage";}

        Optional<Users> user = usersRepository.findById(uid);
        String out = getAllCredsInGroups(uid);
        model.addAttribute("name",user.get().getName());
        model.addAttribute("groupsInfo",out);
        return "group";
    }
    @RequestMapping("/createNewGroup")
    public String sendToCreateGroup(){
        return "createNewGroup";
    }
    @RequestMapping("/myGroup")
    public String sendToGroup(@CookieValue(name = "uid")Long uid,Model model){
        Optional<Users> user = usersRepository.findById(uid);
        String out = getAllCredsInGroups(uid);
        model.addAttribute("name",user.get().getName());
        model.addAttribute("groupsInfo",out);
        return "group";
    }
    @RequestMapping("/addCredential")
    public String addCredential(){
        return "addCredential";
    }
    @RequestMapping("/addUserToGroup")
    public String addUserToGroup(){
        return "addUserToGroup";
    }
    @RequestMapping("/deleteGroup")
    public String deleteGroup(){
        return "deleteGroup";
    }
    @RequestMapping("/addUser")
    public String addUser(@CookieValue(name = "uid")Long uid,
                          @RequestParam(name = "uname")String userToAdd,
                          @RequestParam(name = "gname")String groupToJoin, Model model){
        Optional<Users> userOwner = usersRepository.findById(uid);
        Iterable<Groups> allGroups = groupsRepository.findAll();
        for (Groups g: allGroups) {
            if(g.getGroupName().equals(groupToJoin)){
                if(g.getCreatorUser().equals(userOwner.get().getName())){
                    Iterable<Users> allU = usersRepository.findAll();
                    for (Users u: allU) {
                        if (u.getName().equals(userToAdd)) {
                            UsersGroups nug = new UsersGroups();
                            nug.setUid(u.getUid());
                            nug.setGid(g.getGid());
                            usersGroupsRepository.save(nug);
                            if(!u.isInGroup())u.setInGroup(true);
                            usersRepository.save(u);
                        }
                    }
                }else{
                    model.addAttribute("error","Not Group Creator");
                    return "error";
                }
            }
        }
        String out = getAllCredsInGroups(uid);
        model.addAttribute("name",userOwner.get().getName());
        model.addAttribute("groupsInfo",out);
        return "group";
    }
    @RequestMapping("/deleteUser")
    public String deleteUser(@CookieValue(name="uid")Long uid){

        Iterable<UsersGroups> allUG = usersGroupsRepository.findAll();

        for(UsersGroups ug: allUG){
            if (ug.getUid().equals(uid)){
                usersGroupsRepository.deleteById(ug.getUgid());
            }
        }
        usersRepository.deleteById(uid);
        return "redirect:/";
    }

    @RequestMapping("/delGroup")
    public String delGroup(@CookieValue(name="uid")Long uid, @RequestParam(name="groupName") String groupName, Model model){
        Iterable<UsersGroups> allUG = usersGroupsRepository.findAll();
        Optional<Users> u = usersRepository.findById(uid);
        for(UsersGroups ug: allUG){
            if (ug.getUid().equals(uid)){
               Optional<Groups> g = groupsRepository.findById(ug.getGid());
               if (g.get().getGroupName().equals(groupName)){
                   usersGroupsRepository.deleteById(ug.getUgid());
               }
            }
        }
        String out = getAllCredsInGroups(uid);
        model.addAttribute("name",u.get().getName());
        model.addAttribute("groupsInfo",out);
        return "group";
    }
    @RequestMapping("/newGroup")
    public String createNewGroup(@RequestParam(name="groupName") String groupName,
                                 Model model, @CookieValue(name = "uid")Long uid){
        Iterable<Groups> all = groupsRepository.findAll();
        Optional<Users> user = usersRepository.findById(uid);
        for (Groups g:all) {
            if (groupName.equals(g.getGroupName())){
                model.addAttribute("error","Group Already Exists!");
                return "errorpage";
            }
        }
        Groups g = new Groups();
        g.setGroupName(groupName);
        g.setCreatorUser(user.get().getName());
        groupsRepository.save(g);
        UsersGroups ug = new UsersGroups();
        ug.setGid(g.getGid());
        ug.setUid(uid);
        usersGroupsRepository.save(ug);
        user.get().setInGroup(true);
        usersRepository.save(user.get());
        String out = getAllCredsInGroups(uid);
        model.addAttribute("name",user.get().getName());
        model.addAttribute("groupsInfo",out);
        return "group";
    }
    private String getAllCredsInGroups(Long uid){
        String out = "";
        Iterable<UsersGroups> all = usersGroupsRepository.findAll();
        Iterable<Credentials> allCreds = credentialsRepository.findAll();
        for (UsersGroups g: all) {
            if(g.getUid() == uid){
                Optional<Groups> myG = groupsRepository.findById(g.getGid());
                out += ("Group: "+myG.get().getGroupName()+"\n");
                for (Credentials c:allCreds) {
                    if(c.getGid().equals(myG.get().getGid())){
                        try {
                            out +=
                                    (" \nCredential: " + c.getCredentialName())+
                                            (" \nPassword: " + Encryptor.decrypt("Bar12345Bar12345","RandomInitVector",c.getCredentialPassword()))+
                                            (" \nDescription: "+ c.getDescription()+"\n");
                        }catch (Exception e){

                        }
                    }
                }
            }
        }
        return out;

    }

}
