package GroupProject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableAutoConfiguration
public class GroupApp {

	public static void main(String[] args) {

		SpringApplication.run(GroupApp.class, args);
	}
}

