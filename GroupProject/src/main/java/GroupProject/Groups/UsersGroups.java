package GroupProject.Groups;

import javax.persistence.*;

@Entity

public class UsersGroups {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long ugid;
    @Column(name = "gid", nullable = false)
    private Long gid;
    @Column(name = "uid", nullable = false)
    private Long uid;

    public Long getUgid() {
        return ugid;
    }

    public void setUgid(Long ugid) {
        this.ugid = ugid;
    }

    public Long getGid() {
        return gid;
    }

    public void setGid(Long gid) {
        this.gid = gid;
    }

    public Long getUid() {
        return uid;
    }

    public void setUid(Long uid) {
        this.uid = uid;
    }
}